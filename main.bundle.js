webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_main_main_component__ = __webpack_require__("./src/app/components/main/main.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    { path: 'home', component: __WEBPACK_IMPORTED_MODULE_2__components_main_main_component__["a" /* MainComponent */] },
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: '**', redirectTo: '/home' },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */].forRoot(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = "html {\n    font-size: 14px;\n  }\n  @media (min-width: 768px) {\n    html {\n      font-size: 16px;\n    }\n  }\n  .container {\n    max-width: 960px;\n  }\n  .pricing-header {\n    max-width: 700px;\n  }\n  .card-deck .card {\n    min-width: 220px;\n  }\n  .bd-placeholder-img {\n    font-size: 1.125rem;\n    text-anchor: middle;\n    -webkit-user-select: none;\n    -moz-user-select: none;\n    -ms-user-select: none;\n    user-select: none;\n  }\n  @media (min-width: 768px) {\n    .bd-placeholder-img-lg {\n      font-size: 3.5rem;\n    }\n  }\n  "

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_routing_module__ = __webpack_require__("./src/app/app-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser_animations__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__swimlane_ngx_charts__ = __webpack_require__("./node_modules/@swimlane/ngx-charts/release/esm.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_main_main_component__ = __webpack_require__("./src/app/components/main/main.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_map_map_component__ = __webpack_require__("./src/app/components/map/map.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_barchart_barchart_component__ = __webpack_require__("./src/app/components/barchart/barchart.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_gridboard_gridboard_component__ = __webpack_require__("./src/app/components/gridboard/gridboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_number_card_number_card_component__ = __webpack_require__("./src/app/components/number-card/number-card.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_8__components_main_main_component__["a" /* MainComponent */],
                __WEBPACK_IMPORTED_MODULE_9__components_map_map_component__["a" /* MapComponent */],
                __WEBPACK_IMPORTED_MODULE_10__components_barchart_barchart_component__["a" /* BarchartComponent */],
                __WEBPACK_IMPORTED_MODULE_11__components_gridboard_gridboard_component__["a" /* GridboardComponent */],
                __WEBPACK_IMPORTED_MODULE_12__components_number_card_number_card_component__["a" /* NumberCardComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["a" /* NgbModule */],
                __WEBPACK_IMPORTED_MODULE_4__app_routing_module__["a" /* AppRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_6__swimlane_ngx_charts__["a" /* NgxChartsModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */]
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/barchart/barchart.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/barchart/barchart.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"text-align:center\">\n    <h1 class=\"card-header bg-info\">\n        Welcome to {{ title }}!\n    </h1>\n    <h3 class=\"text-secondary\">\n        PEV Selling Countries\n    </h3>\n    <div class=\"col\">\n        <div class=\"row\">\n            <ngx-charts-bar-vertical [view]=\"view\" [scheme]=\"colorScheme\" [results]=\"single\" [gradient]=\"gradient\"\n                [xAxis]=\"showXAxis\" [yAxis]=\"showYAxis\" [legend]=\"showLegend\" [showXAxisLabel]=\"showXAxisLabel\"\n                [showYAxisLabel]=\"showYAxisLabel\" [xAxisLabel]=\"xAxisLabel\" [yAxisLabel]=\"yAxisLabel\"\n                (select)=\"onSelect($event)\">\n            </ngx-charts-bar-vertical>\n        </div>\n\n        <div class=\"row\">\n            <ngx-charts-bar-vertical-normalized [view]=\"view\" [scheme]=\"colorScheme\" [results]=\"multi\"\n                [gradient]=\"gradient\" [xAxis]=\"showXAxis\" [yAxis]=\"showYAxis\" [legend]=\"showLegend\"\n                [showXAxisLabel]=\"showXAxisLabel\" [showYAxisLabel]=\"showYAxisLabel\" [xAxisLabel]=\"xAxisLabel\"\n                [yAxisLabel]=\"yAxisLabel\" (select)=\"onSelect($event)\">\n            </ngx-charts-bar-vertical-normalized>\n        </div>\n    </div>\n</div>\n\n\n<div class=\"row ml-5\">\n    <h4>\n        Stacked Vertical Bar Chart\n    </h4>\n</div>\n<div class=\"form-group row ml-5\">\n    <ngx-charts-bar-vertical-stacked [view]=\"view\" [scheme]=\"colorScheme\" [results]=\"multi\" [gradient]=\"gradient\"\n        [xAxis]=\"showXAxis\" [yAxis]=\"showYAxis\" [legend]=\"showLegend\" [showXAxisLabel]=\"showXAxisLabel\"\n        [showYAxisLabel]=\"showYAxisLabel\" [xAxisLabel]=\"xAxisLabel\" [yAxisLabel]=\"yAxisLabel\"\n        (select)=\"onSelect($event)\">\n    </ngx-charts-bar-vertical-stacked>\n</div>"

/***/ }),

/***/ "./src/app/components/barchart/barchart.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BarchartComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BarchartComponent = /** @class */ (function () {
    function BarchartComponent() {
        this.title = 'Angular Charts';
        this.view = [600, 400];
        // options for the chart
        this.showXAxis = true;
        this.showYAxis = true;
        this.gradient = false;
        this.showLegend = true;
        this.showXAxisLabel = true;
        this.xAxisLabel = 'Country';
        this.showYAxisLabel = true;
        this.yAxisLabel = 'Sales';
        this.timeline = true;
        this.colorScheme = {
            domain: ['#9370DB', '#87CEFA', '#FA8072', '#FF7F50', '#90EE90', '#9370DB']
        };
        // pie
        this.showLabels = true;
        // data goes here
        this.single = [
            {
                'name': 'China',
                'value': 2243772
            },
            {
                'name': 'USA',
                'value': 1126000
            },
            {
                'name': 'Norway',
                'value': 296215
            },
            {
                'name': 'Japan',
                'value': 257363
            },
            {
                'name': 'Germany',
                'value': 196750
            },
            {
                'name': 'France',
                'value': 204617
            }
        ];
        this.multi = [
            {
                'name': 'China',
                'series': [
                    {
                        'name': '2018',
                        'value': 2243772
                    },
                    {
                        'name': '2017',
                        'value': 1227770
                    }
                ]
            },
            {
                'name': 'USA',
                'series': [
                    {
                        'name': '2018',
                        'value': 1126000
                    },
                    {
                        'name': '2017',
                        'value': 764666
                    }
                ]
            },
            {
                'name': 'Norway',
                'series': [
                    {
                        'name': '2018',
                        'value': 296215
                    },
                    {
                        'name': '2017',
                        'value': 209122
                    }
                ]
            },
            {
                'name': 'Japan',
                'series': [
                    {
                        'name': '2018',
                        'value': 257363
                    },
                    {
                        'name': '2017',
                        'value': 205350
                    }
                ]
            },
            {
                'name': 'Germany',
                'series': [
                    {
                        'name': '2018',
                        'value': 196750
                    },
                    {
                        'name': '2017',
                        'value': 129246
                    }
                ]
            },
            {
                'name': 'France',
                'series': [
                    {
                        'name': '2018',
                        'value': 204617
                    },
                    {
                        'name': '2017',
                        'value': 149797
                    }
                ]
            }
        ];
    }
    BarchartComponent.prototype.ngOnInit = function () {
    };
    BarchartComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-barchart',
            template: __webpack_require__("./src/app/components/barchart/barchart.component.html"),
            styles: [__webpack_require__("./src/app/components/barchart/barchart.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], BarchartComponent);
    return BarchartComponent;
}());



/***/ }),

/***/ "./src/app/components/gridboard/gridboard.component.css":
/***/ (function(module, exports) {

module.exports = ".gradient{\n    border-top: none;\n    /* background: -moz-linear-gradient(top,  rgba(255,255,255,0) 0%, rgba(255,255,255,1) 70%); */\n    /* background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,0)), color-stop(70%,rgba(255,255,255,1))); */\n    /* background: -webkit-linear-gradient(top,  rgba(255,255,255,0) 0%,rgba(255,255,255,1) 70%);\n    background: -o-linear-gradient(top,  rgba(255,255,255,0) 0%,rgba(255,255,255,1) 70%);\n    background: -ms-linear-gradient(top,  rgba(255,255,255,0) 0%,rgba(255,255,255,1) 70%);\n    background: linear-gradient(to bottom,  rgba(255,255,255,0) 0%,rgba(255,255,255,1) 70%); */\n    /* background-color: white; */\n    background-color: #343a40 !important;\n}\n\n.card{\n    margin-top: 10px;\n    /* display: flex; */\n    -ms-flex-direction: column;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n            flex-direction: column;\n    min-width: 0;\n    word-wrap: break-word;\n    /* background-color: white; */\n    background-color: #343a40 !important;\n    /* background-clip: border-box; */\n    border: none;\n    color: azure;\n}"

/***/ }),

/***/ "./src/app/components/gridboard/gridboard.component.html":
/***/ (function(module, exports) {

module.exports = "    <div class=\"row my-3\">\n        <div class=\"col-md-6\">\n            <div class=\"card text-center gradient\">\n                <div class=\"card-block\">\n                    <h4 class=\"card-title\">Connections</h4>\n                    <h2><i class=\"fa fa-plug\"></i></h2>\n                </div>\n                <div class=\"row px-2 no-gutters\">\n                    <div class=\"col-12\">\n                        <h3 class=\"card card-block border-top-0 border-left-0 border-bottom-0\">148,692</h3>\n                    </div>\n                    <!-- <div class=\"col-6\">\n                        <h3 class=\"card card-block border-0\">20%</h3>\n                    </div> -->\n                </div>\n                <p class=\"card-text\">Energy Service Subscriptions</p>\n\n            </div>\n        </div>\n        <div class=\"col-md-6\">\n            <div class=\"card text-center gradient\">\n                <div class=\"card-block\">\n                    <h4 class=\"card-title\">Light Service</h4>\n                    <h2><i class=\"fa fa-lightbulb-o\"></i></h2>\n                </div>\n                <div class=\"row px-2 no-gutters\">\n\n                    <div class=\"col-12\">\n                        <h3 class=\"card card-block border-top-0 border-left-0 border-bottom-0\">382,866</h3>\n                    </div>\n                    <!-- <div class=\"col-6\">\n                        <h3 class=\"card card-block border-0\">20%</h3>\n                    </div> -->\n                </div>\n                <p class=\"card-text\">Candles/Lamps Displaced</p>\n            </div>\n        </div>\n        <div class=\"col-md-6\">\n            <div class=\"card text-center gradient\">\n                <div class=\"card-block\">\n                    <h4 class=\"card-title\">People</h4>\n                    <h2><i class=\"fa fa-users\"></i></h2>\n                </div>\n                <div class=\"row px-2 no-gutters\">\n                    <div class=\"col-12\">\n                        <h3 class=\"card card-block border-top-0 border-left-0 border-bottom-0\">773,198</h3>\n                    </div>\n                    <!-- <div class=\"col-6\">\n                        <h3 class=\"card card-block border-0\">20%</h3>\n                    </div> -->\n                </div>\n                <p class=\"card-text\">Beneficiaries</p>\n            </div>\n        </div>\n        <div class=\"col-md-6\">\n            <div class=\"card text-center gradient\">\n                <div class=\"card-block\">\n                    <h4 class=\"card-title\">Gender</h4>\n                    <h2><i class=\"fa fa-female\"></i></h2>\n                </div>\n                <div class=\"row px-2 no-gutters\">\n                    <div class=\"col-12\">\n                        <h3 class=\"card card-block border-top-0 border-left-0 border-bottom-0\">34,990</h3>\n                    </div>\n                    <!-- <div class=\"col-6\">\n                        <h3 class=\"card card-block border-0\">20%</h3>\n                    </div> -->\n                </div>\n                <p class=\"card-text\">Women Primary Customers</p>\n            </div>\n        </div>\n        <div class=\"col-md-6\">\n            <div class=\"card text-center gradient\">\n                <div class=\"card-block\">\n                    <h4 class=\"card-title\">Jobs</h4>\n                    <h2><i class=\"fa fa-briefcase\"></i></h2>\n                </div>\n                <div class=\"row px-2 no-gutters\">\n                    <div class=\"col-12\">\n                        <h3 class=\"card card-block border-top-0 border-left-0 border-bottom-0\">1,505</h3>\n                    </div>\n                    <!-- <div class=\"col-6\">\n                        <h3 class=\"card card-block border-0\">20%</h3>\n                    </div> -->\n                </div>\n                <p class=\"card-text\">Full & Part Time Jobs</p>\n            </div>\n        </div>\n        <div class=\"col-md-6\">\n            <div class=\"card text-center gradient\">\n                <div class=\"card-block\">\n                    <h4 class=\"card-title\">Climate</h4>\n                    <h2><i class=\"fa fa-globe\"></i></h2>\n                </div>\n                <div class=\"row px-2 no-gutters\">\n                    <div class=\"col-12\">\n                        <h3 class=\"card card-block border-top-0 border-left-0 border-bottom-0\">148,692</h3>\n                    </div>\n                    <!-- <div class=\"col-6\">\n                        <h3 class=\"card card-block border-0\">20%</h3>\n                    </div> -->\n                </div>\n                <p class=\"card-text\">Kg of C02 Mitigated Annually</p>\n            </div>\n        </div>\n        <div class=\"col-md-6\">\n            <div class=\"card text-center gradient\">\n                <div class=\"card-block\">\n                    <h4 class=\"card-title\">Power</h4>\n                    <h2><i class=\"fa fa-battery-full\"></i></h2>\n                </div>\n                <div class=\"row px-2 no-gutters\">\n                    <div class=\"col-12\">\n                        <h3 class=\"card card-block border-top-0 border-left-0 border-bottom-0\">3,505</h3>\n                    </div>\n                    <!-- <div class=\"col-6\">\n                        <h3 class=\"card card-block border-0\">20%</h3>\n                    </div> -->\n                </div>\n                <p class=\"card-text\">In 000 Watts</p>\n            </div>\n        </div>\n        <div class=\"col-md-6\">\n            <div class=\"card text-center gradient\">\n                <div class=\"card-block\">\n                    <h4 class=\"card-title\">Finance</h4>\n                    <h2><i class=\"fa fa-money\"></i></h2>\n                </div>\n                <div class=\"row px-2 no-gutters\">\n                    <div class=\"col-12\">\n                        <h3 class=\"card card-block border-top-0 border-left-0 border-bottom-0\">34.8M</h3>\n                    </div>\n                    <!-- <div class=\"col-6\">\n                        <h3 class=\"card card-block border-0\">20%</h3>\n                    </div> -->\n                </div>\n                <p class=\"card-text\">Additonal USD funds</p>\n            </div>\n        </div>\n    </div>"

/***/ }),

/***/ "./src/app/components/gridboard/gridboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GridboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var GridboardComponent = /** @class */ (function () {
    function GridboardComponent() {
    }
    GridboardComponent.prototype.ngOnInit = function () {
    };
    GridboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-gridboard',
            template: __webpack_require__("./src/app/components/gridboard/gridboard.component.html"),
            styles: [__webpack_require__("./src/app/components/gridboard/gridboard.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], GridboardComponent);
    return GridboardComponent;
}());



/***/ }),

/***/ "./src/app/components/main/main.component.css":
/***/ (function(module, exports) {

module.exports = ".content-container {\n    vertical-align: middle;\n    width: 30%;\n    display: inline-block;\n    height: 50%;\n    position: absolute;\n    top: 0;\n    bottom: 0%;\n    right: 0;\n    margin: auto;\n    margin-top: 3%\n}"

/***/ }),

/***/ "./src/app/components/main/main.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- Navigation -->\n<!-- <nav class=\"navbar navbar-expand-lg navbar-light bg-light shadow fixed-top\"> -->\n<nav class=\"navbar navbar-expand-lg navbar-light bg-light shadow fixed-top\">\n  <div class=\"container\">\n    <a class=\"navbar-brand\" href=\"#\">EDISON II</a>\n    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\"\n      aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n      <span class=\"navbar-toggler-icon\"></span>\n    </button>\n    <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">\n      <ul class=\"navbar-nav ml-auto\">\n        <li class=\"nav-item active\">\n          <a class=\"nav-link\" href=\"#\">Admin\n            <span class=\"sr-only\">(current)</span>\n          </a>\n        </li>\n        <li class=\"nav-item\">\n          <a class=\"nav-link\" href=\"#\">Privacy</a>\n        </li>\n      </ul>\n    </div>\n  </div>\n</nav>\n\n\n<app-map> </app-map>\n\n<!-- Full Page Image Header with Vertically Centered Content -->\n<div class=\"content-container\">\n  <div class=\"container-fluid h-100 \">\n    <app-gridboard></app-gridboard>\n    <!-- <app-number-card></app-number-card> -->\n  </div>\n</div>\n\n<!-- Page Content -->\n<section class=\"py-5\">\n  <div class=\"container\">\n    <h2 class=\"font-weight-light\">EDISON</h2>\n    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus ab nulla dolorum autem nisi officiis\n      blanditiis voluptatem hic, assumenda aspernatur facere ipsam nemo ratione cumque magnam enim fugiat reprehenderit\n      expedita.</p>\n\n      <!-- <app-barchart></app-barchart> -->\n\n  </div>\n</section>"

/***/ }),

/***/ "./src/app/components/main/main.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MainComponent = /** @class */ (function () {
    function MainComponent() {
    }
    MainComponent.prototype.ngOnInit = function () {
    };
    MainComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-main',
            template: __webpack_require__("./src/app/components/main/main.component.html"),
            styles: [__webpack_require__("./src/app/components/main/main.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MainComponent);
    return MainComponent;
}());



/***/ }),

/***/ "./src/app/components/map/map.component.css":
/***/ (function(module, exports) {

module.exports = "  .match-parent {\n    height: 95vh;\n    min-height: 500px;\n    background-size: cover;\n    background-position: center;\n    background-repeat: no-repeat;\n    /* height: auto !important;  */\n    /* min-height: 100%;  */\n  }\n  \n  #key {\n    background-color: rgba(0, 0, 0, 0.8);\n    width: 22.22%;\n    height: auto;\n    overflow: auto;\n    position: absolute;\n    top: 0;\n    left: 0;\n    margin-top: 4%;\n    color: white;\n  }\n  \n  .total {\n    font-family: 'Montserrat', sans-serif;\n    font-weight: 800;\n    font-size: 15px;\n  }\n  \n  .table_preview {\n    font-family: 'Montserrat', sans-serif;\n    color: white !important;\n    border-collapse: collapse;\n  }\n  \n  .table {\n    width: 100%;\n    margin-bottom: 1rem;\n    color: #ffffff;\n  }\n  \n  table > thead > tr > td {\n    border-bottom: 1px solid red !important;\n    color: white !important;\n  }\n  \n  .pie {\n    cursor: pointer;\n  }"

/***/ }),

/***/ "./src/app/components/map/map.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"map\" id=\"map\" class=\"match-parent\"></div>\n<div id=\"key\"></div>\n"

/***/ }),

/***/ "./src/app/components/map/map.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_mapbox_gl__ = __webpack_require__("./node_modules/mapbox-gl/dist/mapbox-gl.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_mapbox_gl___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_mapbox_gl__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_d3__ = __webpack_require__("./node_modules/d3/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MapComponent = /** @class */ (function () {
    function MapComponent() {
        this.style = 'mapbox://styles/saminated/ck1lnoig207hl1cqsekeahyy2';
        // style = 'mapbox://styles/mapbox/dark-v10';
        this.lat = 37.75;
        this.lng = -122.41;
    }
    MapComponent.prototype.ngOnInit = function () {
    };
    MapComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.lat = -14.5186239;
        this.lng = 27.5599164;
        var start = [-74.50, 40];
        var end = [27.5599164, -14.5186239];
        // https://stackoverflow.com/questions/44332290/mapbox-gl-typing-wont-allow-accesstoken-assignment
        __WEBPACK_IMPORTED_MODULE_2_mapbox_gl__["accessToken"] = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].mapbox.accessToken;
        this.map = new __WEBPACK_IMPORTED_MODULE_2_mapbox_gl__["Map"]({
            container: 'map',
            style: this.style,
            zoom: 4,
            center: end
        });
        // colors setup
        // tslint:disable-next-line: max-line-length
        var colors = ['#8dd3c7', '#ffffb3', '#bebada', '#fb8072', '#80b1d3', '#fdb462', '#b3de69', '#fccde5', '#d9d9d9', '#bc80bd', '#ccebc5'];
        var colorScale = __WEBPACK_IMPORTED_MODULE_3_d3__["i" /* scaleOrdinal */]()
            .domain(['tier1', 'tier2', 'tier3', 'tier4', 'tier5', 'tier6', 'tier7', 'tier8', 'tier9', 'tier10', 'tier11'])
            .range(colors);
        var tier1 = ['==', ['get', 'tier'], '1'];
        var tier2 = ['==', ['get', 'tier'], '2'];
        var tier3 = ['==', ['get', 'tier'], '3'];
        var tier4 = ['==', ['get', 'tier'], '4'];
        var tier5 = ['==', ['get', 'tier'], '5'];
        var tier6 = ['==', ['get', 'tier'], '6'];
        var tier7 = ['==', ['get', 'tier'], '7'];
        var tier8 = ['==', ['get', 'tier'], '8'];
        var tier9 = ['==', ['get', 'tier'], '9'];
        var tier10 = ['==', ['get', 'tier'], '10'];
        this.map.on('load', function () {
            _this.map.flyTo({
                // These options control the ending camera position: centered at
                // the target, at zoom level 9, and north up.
                center: end,
                zoom: 9,
                bearing: 0,
                // These options control the flight curve, making it move
                // slowly and zoom out almost completely before starting
                // to pan.
                speed: 0.2,
                curve: 1,
                // This can be any easing function: it takes a number between
                // 0 and 1 and returns another number between 0 and 1.
                easing: function (t) { return t; }
            });
            _this.map.addSource('edisonapi', {
                type: 'geojson',
                // data: 'https://docs.mapbox.com/mapbox-gl-js/assets/earthquakes.geojson',
                // data: 'http://127.0.0.1:5000/api/geojson',
                data: 'http://3.15.146.54/api/geojson',
                cluster: true,
                clusterMaxZoom: 14,
                clusterRadius: 50,
                clusterProperties: {
                    'tier1': ['+', ['case', tier1, 1, 0]],
                    'tier2': ['+', ['case', tier2, 1, 0]],
                    'tier3': ['+', ['case', tier3, 1, 0]],
                    'tier4': ['+', ['case', tier4, 1, 0]],
                    'tier5': ['+', ['case', tier5, 1, 0]],
                    'tier6': ['+', ['case', tier6, 1, 0]],
                    'tier7': ['+', ['case', tier7, 1, 0]],
                    'tier8': ['+', ['case', tier8, 1, 0]],
                    'tier9': ['+', ['case', tier9, 1, 0]],
                    'tier10': ['+', ['case', tier10, 1, 0]],
                }
            });
            var current_fuel = 'tier3';
            // // circle clusters
            // this.map.addLayer({
            //   id: 'clusters',
            //   type: 'circle',
            //   source: 'edisonapi',
            //   filter: [
            //     'all',
            //     ['>', ['get', current_fuel], 1],
            //     ['==', ['get', 'cluster'], true]
            //   ],
            //   paint: {
            //     'circle-color': 'rgba(0,0,0,.6)',
            //     'circle-radius': [
            //       'step',
            //       ['get', current_fuel],
            //       20,
            //       100,
            //       30,
            //       750,
            //       40
            //     ],
            //     'circle-stroke-color': '#8dd3c7',
            //     'circle-stroke-width': 5
            //   }
            // });
            // // circle cluster symbol text
            // this.map.addLayer({
            //   id: 'cluster-count',
            //   type: 'symbol',
            //   source: 'edisonapi',
            //   filter: [
            //     'all',
            //     ['>', ['get', current_fuel], 1],
            //     ['==', ['get', 'cluster'], true]
            //   ],
            //   layout: {
            //     'text-field': ['number-format', ['get', current_fuel], {}],
            //     'text-font': ['Montserrat Bold', 'Arial Unicode MS Bold'],
            //     'text-size': 13
            //   },
            //   paint: {
            //     'text-color': '#8dd3c7'
            //   }
            // });
            // unclustered points individual
            _this.map.addLayer({
                'id': 'unclustered-point',
                'type': 'circle',
                'source': 'edisonapi',
                'filter': ['!=', ['get', 'cluster'], true],
                'paint': {
                    'circle-color': ['case',
                        tier1, colorScale('tier1'),
                        tier2, colorScale('tier2'),
                        tier3, colorScale('tier3'),
                        tier4, colorScale('tier4'),
                        tier5, colorScale('tier5'),
                        tier6, colorScale('tier6'),
                        tier7, colorScale('tier7'),
                        tier8, colorScale('tier8'),
                        tier9, colorScale('tier9'),
                        tier10, colorScale('tier10'),
                        '#ffed6f'],
                    'circle-radius': 5
                }
            });
            // unclustered points individual outer
            _this.map.addLayer({
                'id': 'unclustered-point-outer',
                'type': 'circle',
                'source': 'edisonapi',
                'filter': ['!=', ['get', 'cluster'], true],
                'paint': {
                    'circle-stroke-color': ['case',
                        tier1, colorScale('tier1'),
                        tier2, colorScale('tier2'),
                        tier3, colorScale('tier3'),
                        tier4, colorScale('tier4'),
                        tier5, colorScale('tier5'),
                        tier6, colorScale('tier6'),
                        tier7, colorScale('tier7'),
                        tier8, colorScale('tier8'),
                        tier9, colorScale('tier9'),
                        tier10, colorScale('tier10'),
                        '#ffed6f'],
                    'circle-stroke-width': 2,
                    'circle-radius': 10,
                    'circle-color': 'rgba(0, 0, 0, 0)'
                }
            });
            var markers = {};
            var markersOnScreen = {};
            var point_counts = [];
            var totals;
            var updateMarkers = function () {
                // hide preview
                document.getElementById('key').innerHTML = '';
                // keep track of new markers
                var newMarkers = {};
                // get the features whether or not they are visible (https://docs.mapbox.com/mapbox-gl-js/api/#map#queryrenderedfeatures)
                var features = _this.map.querySourceFeatures('edisonapi');
                totals = getPointCount(features);
                // loop through each feature
                features.forEach(function (feature) {
                    var coordinates = feature.geometry.coordinates;
                    // get our properties, which include our clustered properties
                    var props = feature.properties;
                    // continue only if the point is part of a cluster
                    if (!props.cluster) {
                        return;
                    }
                    // if yes, get the cluster_id
                    var id = props.cluster_id;
                    // create a marker object with the cluster_id as a key
                    var marker = markers[id];
                    // if that marker doesn't exist yet, create it
                    if (!marker) {
                        // create an html element (more on this later)
                        var el = createDonutChart(props, totals);
                        // create the marker object passing the html element and the coordinates
                        marker = markers[id] = new __WEBPACK_IMPORTED_MODULE_2_mapbox_gl__["Marker"]({
                            element: el
                        }).setLngLat(coordinates);
                    }
                    // create an object in our newMarkers object with our current marker representing the current cluster
                    newMarkers[id] = marker;
                    // if the marker isn't already on screen then add it to the map
                    if (!markersOnScreen[id]) {
                        marker.addTo(_this.map);
                    }
                });
                // check if the marker with the cluster_id is already on the screen by iterating through our markersOnScreen object, which keeps track of that
                for (var id in markersOnScreen) {
                    // if there isn't a new marker with that id, then it's not visible, therefore remove it.
                    if (!newMarkers[id]) {
                        markersOnScreen[id].remove();
                    }
                }
                // otherwise, it is visible and we need to add it to our markersOnScreen object
                markersOnScreen = newMarkers;
            };
            var getPointCount = function (features) {
                features.forEach(function (f) {
                    if (f.properties.cluster) {
                        point_counts.push(f.properties.point_count);
                    }
                });
                return point_counts;
            };
            var createDonutChart = function (props, totals) {
                // create a div element to hold our marker
                var div = document.createElement('div');
                // create our array with our data
                var data = [
                    { type: 'tier1', count: props.tier1 },
                    { type: 'tier2', count: props.tier2 },
                    { type: 'tier3', count: props.tier3 },
                    { type: 'tier4', count: props.tier4 },
                    { type: 'tier5', count: props.tier5 },
                    { type: 'tier6', count: props.tier6 },
                    { type: 'tier7', count: props.tier7 },
                    { type: 'tier8', count: props.tier8 },
                    { type: 'tier9', count: props.tier9 },
                    { type: 'tier10', count: props.tier10 },
                ];
                // svg config
                var thickness = 10;
                var scale = __WEBPACK_IMPORTED_MODULE_3_d3__["h" /* scaleLinear */]()
                    .domain([__WEBPACK_IMPORTED_MODULE_3_d3__["f" /* min */](totals), __WEBPACK_IMPORTED_MODULE_3_d3__["e" /* max */](totals)])
                    .range([500, __WEBPACK_IMPORTED_MODULE_3_d3__["e" /* max */](totals)]);
                // calculate the radius
                var radius = Math.sqrt(scale(props.point_count));
                // calculate the radius of the smaller circle
                var circleRadius = radius - thickness;
                // create the svg
                var svg = __WEBPACK_IMPORTED_MODULE_3_d3__["j" /* select */](div)
                    .append('svg')
                    .attr('class', 'pie')
                    .attr('width', radius * 2)
                    .attr('height', radius * 2);
                // create a group to hold our arc paths and center
                var g = svg.append('g')
                    .attr('transform', "translate(" + radius + ", " + radius + ")");
                // create an arc using the radius above
                var arc = __WEBPACK_IMPORTED_MODULE_3_d3__["a" /* arc */]()
                    .innerRadius(radius - thickness)
                    .outerRadius(radius);
                // create the pie for the donut
                var pie = __WEBPACK_IMPORTED_MODULE_3_d3__["g" /* pie */]()
                    .value(function (d) { return d.count; })
                    .sort(null);
                // using the pie and the arc, create our path based on the data
                var path = g.selectAll('path')
                    .data(pie(data.sort(function (x, y) { return __WEBPACK_IMPORTED_MODULE_3_d3__["b" /* ascending */](y.count, x.count); })))
                    .enter()
                    .append('path')
                    .attr('d', arc)
                    .attr('fill', function (d) { return colorScale(d.data.type); });
                // create the center circle
                var circle = g.append('circle')
                    .attr('r', circleRadius)
                    .attr('fill', 'rgba(0, 0, 0, 0.7)')
                    .attr('class', 'center-circle');
                // create the text
                var text = g
                    .append('text')
                    .attr('class', 'total')
                    .text(props.point_count_abbreviated)
                    .attr('text-anchor', 'middle')
                    .attr('dy', 5)
                    .attr('fill', 'white');
                var infoEl = createTable(props);
                svg.on('click', function () {
                    __WEBPACK_IMPORTED_MODULE_3_d3__["k" /* selectAll */]('.center-circle').attr('fill', 'rgba(0, 0, 0, 0.7)');
                    circle.attr('fill', 'rgb(71, 79, 102)');
                    document.getElementById('key').innerHTML = '';
                    document.getElementById('key').appendChild(infoEl);
                });
                return div;
            };
            var createTable = function (props) {
                var getPerc = function (count) {
                    return count / props.point_count;
                };
                var data = [
                    { type: 'tier1', perc: getPerc(props.tier1) },
                    { type: 'tier2', perc: getPerc(props.tier2) },
                    { type: 'tier3', perc: getPerc(props.tier3) },
                    { type: 'tier4', perc: getPerc(props.tier4) },
                    { type: 'tier5', perc: getPerc(props.tier5) },
                    { type: 'tier6', perc: getPerc(props.tier6) },
                    { type: 'tier7', perc: getPerc(props.tier7) },
                    { type: 'tier8', perc: getPerc(props.tier8) },
                    { type: 'tier9', perc: getPerc(props.tier9) },
                    { type: 'tier10', perc: getPerc(props.tier10) },
                ];
                var columns = ['type', 'perc'];
                var div = document.createElement('div');
                var table = __WEBPACK_IMPORTED_MODULE_3_d3__["j" /* select */](div).append('table').attr('class', 'table table-dark');
                var thead = table.append('thead');
                var tbody = table.append('tbody');
                thead.append('tr')
                    .selectAll('th')
                    .data(columns).enter()
                    .append('th')
                    .text(function (d) {
                    var colName = d === 'perc' ? '%' : 'Tier Type';
                    return colName;
                });
                var rows = tbody.selectAll('tr')
                    .data(data.filter(function (i) { return i.perc; }).sort(function (x, y) { return __WEBPACK_IMPORTED_MODULE_3_d3__["c" /* descending */](x.perc, y.perc); }))
                    .enter()
                    .append('tr')
                    .style('border-left', function (d) { return "20px solid " + colorScale(d.type); });
                // create a cell in each row for each column
                var cells = rows.selectAll('td')
                    .data(function (row) {
                    return columns.map(function (column) {
                        var val = column === 'perc' ? __WEBPACK_IMPORTED_MODULE_3_d3__["d" /* format */]('.2%')(row[column]) : row[column];
                        return { column: column, value: val };
                    });
                })
                    .enter()
                    .append('td')
                    .text(function (d) { return d.value; })
                    .style('text-transform', 'capitalize');
                return div;
            };
            _this.map.on('data', function (e) {
                if (e.sourceId !== 'edisonapi' || !e.isSourceLoaded) {
                    return;
                }
                _this.map.on('move', updateMarkers);
                _this.map.on('moveend', updateMarkers);
                updateMarkers();
            });
        });
    };
    MapComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-map',
            template: __webpack_require__("./src/app/components/map/map.component.html"),
            styles: [__webpack_require__("./src/app/components/map/map.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MapComponent);
    return MapComponent;
}());



/***/ }),

/***/ "./src/app/components/number-card/number-card.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/number-card/number-card.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <p>\n  number-card works!\n</p> -->\n<div class=\"row\">\n  <ngx-charts-number-card [view]=\"view\" [scheme]=\"colorScheme\" [results]=\"single\" (select)=\"onSelect($event)\">\n  </ngx-charts-number-card>\n</div>"

/***/ }),

/***/ "./src/app/components/number-card/number-card.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NumberCardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NumberCardComponent = /** @class */ (function () {
    function NumberCardComponent() {
        this.title = 'Angular Charts';
        this.view = [600, 400];
        // options for the chart
        this.showXAxis = true;
        this.showYAxis = true;
        this.gradient = false;
        this.showLegend = true;
        this.showXAxisLabel = true;
        this.xAxisLabel = 'Country';
        this.showYAxisLabel = true;
        this.yAxisLabel = 'Sales';
        this.timeline = true;
        this.colorScheme = {
            domain: ['#9370DB', '#87CEFA', '#FA8072', '#FF7F50', '#90EE90', '#9370DB']
        };
        // pie
        this.showLabels = true;
        // data goes here
        this.single = [
            {
                'name': 'China',
                'value': 2243772
            },
            {
                'name': 'USA',
                'value': 1126000
            },
            {
                'name': 'Norway',
                'value': 296215
            },
            {
                'name': 'Japan',
                'value': 257363
            },
            {
                'name': 'Germany',
                'value': 196750
            },
            {
                'name': 'France',
                'value': 204617
            }
        ];
        this.multi = [
            {
                'name': 'China',
                'series': [
                    {
                        'name': '2018',
                        'value': 2243772
                    },
                    {
                        'name': '2017',
                        'value': 1227770
                    }
                ]
            },
            {
                'name': 'USA',
                'series': [
                    {
                        'name': '2018',
                        'value': 1126000
                    },
                    {
                        'name': '2017',
                        'value': 764666
                    }
                ]
            },
            {
                'name': 'Norway',
                'series': [
                    {
                        'name': '2018',
                        'value': 296215
                    },
                    {
                        'name': '2017',
                        'value': 209122
                    }
                ]
            },
            {
                'name': 'Japan',
                'series': [
                    {
                        'name': '2018',
                        'value': 257363
                    },
                    {
                        'name': '2017',
                        'value': 205350
                    }
                ]
            },
            {
                'name': 'Germany',
                'series': [
                    {
                        'name': '2018',
                        'value': 196750
                    },
                    {
                        'name': '2017',
                        'value': 129246
                    }
                ]
            },
            {
                'name': 'France',
                'series': [
                    {
                        'name': '2018',
                        'value': 204617
                    },
                    {
                        'name': '2017',
                        'value': 149797
                    }
                ]
            }
        ];
    }
    NumberCardComponent.prototype.ngOnInit = function () {
    };
    NumberCardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-number-card',
            template: __webpack_require__("./src/app/components/number-card/number-card.component.html"),
            styles: [__webpack_require__("./src/app/components/number-card/number-card.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], NumberCardComponent);
    return NumberCardComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    mapbox: {
        accessToken: 'pk.eyJ1Ijoic2FtaW5hdGVkIiwiYSI6ImNrMWxubHpzYzA3M2UzbnBndWUwb3ppN24ifQ._2rGOvc6XcU9HgHnypPopQ'
    }
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_15" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map